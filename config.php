<?php

return [
  'db' => [
    'host' => 'localhost',
    'user' => 'alumno',
    'pass' => 'qwas123',
    'name' => 'empresasdb',
    'options' => [
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ]
  ]
];